How to contribute?
==================

Everyone is invited to contribute to the game "Pekka Kana 2" by submitting
bug reports, testing on different platforms, writing examples,
improving documentation, profiling and optimizing, helping newbies,
telling others about the game, etc.

**When contributing to the "Pekka Kana 2" project you must agree to
the BSD 2-clause licensing terms. You can find the complete text
in the file [LICENSE](LICENSE).**

**Submitting bugs**
-------------------

1. Make sure you have or create a GIT repository account.
2. Submit your issue (make sure the same issue does not exist yet).
3. Try to clearly describe the problem and include steps to reproduce
when it is an error.
4. Create a topic branch from which you want to base your work.
5. Name your affiliate with the type of problem you are correcting feat, task,
documents.
6. Avoid working directly at your main affiliate.

Thank you for your interest!
